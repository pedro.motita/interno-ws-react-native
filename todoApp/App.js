import React, { useState } from 'react';
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  Image,
} from 'react-native';
import Tarefa from './Tarefa';
import bota from './assets/images/bota.jpg';

function App() {

  const [tarefas, setTarefas] = useState([])
  
  function handleAddTarefa() {
    const tarefasNovas = [
      ...tarefas,
      {
        title: 'tarefa nova',
        id: Math.random(),
      }
    ];

    setTarefas(tarefasNovas);
  }

  function deletaTarefa(id) {
    const tarefasIncompletas = tarefas.filter((elemento) => {
      if (elemento.id !== id)
        return elemento;
    });

    setTarefas(tarefasIncompletas);
  }

  return (
    <View style={{ flex: 1 }}>
      { tarefas.length === 0 ?
          <Text style={{ fontSize: 24 }}>Parabéns, não há tarefas</Text>
        :
          <FlatList
            data={tarefas}
            renderItem={({item}) => (
              <Tarefa
                title={item.title}
                handleBotaPress={() => deletaTarefa(item.id)}
              />
            )}
            keyExtractor={item => `key igual a ${item.id}`}
          />
      }
      <TouchableOpacity onPress={handleAddTarefa} style={{ position: 'absolute', bottom: 30, right: 30 }}>
        <Image source={bota} style={{ width:  60, height: 60}}/>
      </TouchableOpacity>
    </View>
  );
}

export default App;