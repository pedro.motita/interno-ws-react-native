import React from 'react';
import bota from './assets/images/bota.jpg';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';

function Tarefa(props) {
  return (
    <View style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
      <TouchableOpacity onPress={props.handleBotaPress}>
        <Image
          style={{ width: 60, height: 60, marginRight: 20 }}
          source={bota}
        />
      </TouchableOpacity>
      <Text style={{fontSize: 24}}>{ props.title }</Text>
    </View>  
  );
}

export default Tarefa;